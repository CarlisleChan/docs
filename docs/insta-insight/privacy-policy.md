# Privacy Policy

**Updated Date:** 2023/11/19

**Effective Date:** 2023/11/19

## Preamble

_InstaInsight_ is a product provided by _Miiluu_ (hereinafter referred to as "we"). When using our services, we may collect and use relevant information about you. Through this "Privacy Policy," we aim to explain to you how we collect, use, store, and share this information when you use our services, as well as how we provide access, updates, controls, and protection for this information. This "Privacy Policy" is closely related to your use of the _InstaInsight_ service. We hope you read it carefully and make appropriate choices as guided by this "Privacy Policy." Technical terms mentioned in this "Privacy Policy" are presented concisely, with further explanations provided through links for your understanding.

**By using or continuing to use our services, you agree that we collect, use, store, and share your relevant information according to this "Privacy Policy."**

If you have any questions about this "Privacy Policy" or related matters, please contact us at **insta-insight@miiluu.xyz**.

## 1. Information We Collect

When we or our third-party partners provide services, we may collect, store, and use information related to you. If you do not provide relevant information, you may not be able to register as our user, enjoy certain services, or achieve the intended effects of related services.

- **Log Information**: This refers to information collected during your use of our services through cookies, identifiers, and related technologies. It includes your **device information**, **browsing information**, and **click information**. This information is stored as log information to provide you with a personalized user experience and ensure service security. You can manage or reject the use of cookies, identifiers, or related technologies through browser settings.

## 2. Information Storage

**2.1 Storage Methods and Duration**

- We will securely store your information, including local storage (such as data caching using the app), databases, and server logs.
- Generally, we will only store your personal information for the necessary time to achieve the service purposes or as required by laws and regulations.

**2.2 Geographic Storage**

- In accordance with laws and regulations, we will store user personal information collected domestically in China.
- Currently, we do not transfer or store your personal information across borders. If there is a need for cross-border transmission or storage in the future, we will inform you of the purpose, recipient, security measures, and risks involved in the information exit, and obtain your consent.

**2.3 Notice of Product or Service Termination**

- In the event of our product or service ceasing operation, we will notify you through push notifications, announcements, etc., and delete your personal information or anonymize it within a reasonable period, except as otherwise provided by laws and regulations.

## 3. Information Security

We use various security technologies and procedures to prevent the loss, improper use, unauthorized access, or disclosure of information. For example, in certain services, we will use encryption technology (such as SSL) to protect the personal information you provide. However, please understand that due to technical limitations and possible malicious means, even with strengthened security measures in the internet industry, it is not always possible to guarantee 100% security of information. You need to be aware that problems may arise in the systems and communication networks you use to access our services due to factors beyond our control.

## 4. How We Use Information

We may use the information collected during the provision of services for the following purposes:

- Providing services to you.
- Identity verification, customer service, security, fraud monitoring, archiving, and backup purposes during the provision of services to ensure the security of the products and services we provide to you.
- Assisting in designing new services and improving our existing services.
- Understanding how you access and use our services to respond to your personalized needs, such as language settings, location settings, personalized help services, and instructions, or other responses to you and other users.
- Providing you with more relevant advertising than general advertising.
- Evaluating the effectiveness of advertisements and other promotional activities and improving them.
- Software authentication or software upgrades.
- Allowing you to participate in surveys related to our products and services.

## 5. Information Sharing

Currently, we do not proactively share or transfer your personal information to third parties. If there is other sharing or transfer of your personal information, or if you need us to share or transfer your personal information to a third party, we will directly or confirm with the third party to obtain your explicit consent.

For advertising purposes, assessing and optimizing advertising effectiveness, etc., we need to share some of your data with third-party partners such as advertisers and their agents. We require them to strictly comply with our measures and requirements regarding data privacy protection, including but not limited to processing according to data protection agreements, commitments, and related data processing policies, avoiding identification of personal identities, and ensuring privacy security.

We will not share information that can be used to identify your personal identity (such as your name or email address) with partners unless you explicitly authorize it.

We will not publicly disclose the collected personal information. If public disclosure is necessary, we will inform you of the purpose of the disclosure, the types of information disclosed, and any sensitive information involved, and obtain your explicit consent.

As our business continues to develop, we may engage in transactions such as mergers, acquisitions, and asset transfers. We will inform you of relevant situations and continue to protect your personal information in accordance with legal regulations and standards not lower than this "Privacy Policy."

In addition, according to relevant laws, regulations, and national standards, we may share, transfer, or publicly disclose personal information without obtaining your prior consent in the following situations:

- Directly related to national security and defense security.
- Directly related to public safety, public health, and major public interests.
- Directly related to criminal investigation, prosecution, trial, and execution of judgments.
- Necessary for the protection of the life, property, etc., of the personal information subject or other individuals but difficult to obtain the consent of the individual.
- Personal information subjects voluntarily disclose their personal information to the public.
- Collection of personal information from legally publicly disclosed information, such as lawful news reports, government information disclosure, and other channels.

## 6. Your Rights

During your use of our services, we may provide corresponding operation settings based on the specific situation of the product so that you can query, delete, correct, or withdraw your relevant personal information. You can refer to specific instructions for operation. In addition, we have also established channels for complaints and reports, and your opinions will be processed promptly. If you are unable to exercise your personal information subject rights through the above methods, you can submit your request through the contact information provided in this "Privacy Policy," and we will provide feedback in accordance with legal regulations.

## 7. Changes

We may revise the terms of this "Privacy Policy" from time to time. When changes occur, we will prompt you with the new "Privacy Policy" during version updates and explain the effective date to you. Please carefully read the changed content of the "Privacy Policy." **If you continue to use our services, it means you agree that we handle your personal information according to the updated "Privacy Policy."**

## 8. Protection of Minors

We encourage parents or guardians to guide individuals under the age of eighteen in using our services. We recommend that minors encourage their parents or guardians to read this "Privacy Policy" and seek their consent and guidance before submitting personal information.

