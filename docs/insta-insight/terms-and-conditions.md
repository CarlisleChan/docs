# User Agreement

_Miiluu_ (hereinafter referred to as "we") provides the _InstaInsight_ service to users (hereinafter referred to as "you") according to this agreement. This agreement is legally binding for both you and us.

#### 1. Functions of the Service

You can use this service for fan analysis, post analysis, snapshot analysis, etc.

#### 2. Scope of Responsibility and Limitations

The results obtained from using this service are for reference only, and the official information prevails.

#### 3. Privacy Protection

We value the protection of your privacy. Your personal information will be protected and regulated according to the "Privacy Policy," details of which can be found in the "Privacy Policy" section.

#### 4. Other Terms

4.1 The titles of all the terms in this agreement are for reading convenience only and have no substantive meaning in themselves. They cannot be used as a basis for interpreting the meaning of this agreement.

4.2 If any part of the terms of this agreement is invalid or unenforceable for any reason, the remaining terms are still valid and binding on both parties.
